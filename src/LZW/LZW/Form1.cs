﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LZW
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string fromFile;
        private string toFile;
        private LZW lzw = new LZW();

        private void button1_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                fromFile = dlg.FileName;
                textBox1.Text = fromFile;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var dlg = new SaveFileDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                toFile = dlg.FileName;
                textBox2.Text = toFile;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var uncompressed = File.ReadAllBytes(fromFile);
            var compressed = lzw.CompressBytes(uncompressed);
            File.WriteAllBytes(toFile, compressed);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var compressed = File.ReadAllBytes(fromFile);
            var uncompressed = lzw.DecompressBytes(compressed);
            File.WriteAllBytes(toFile, uncompressed);
        }
    }
}
