﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZW
{
    class LZW
    {
        public byte[] CompressBytes(byte[] uncompressed)
        {
            List<int> compressedInts = Compress(uncompressed);
            
            var compressedBits = IntsToBits(compressedInts);
            var comressedBytes = ToByteArray(new BitArray(compressedBits.ToArray()));
            return comressedBytes;
        }

        public byte[] DecompressBytes(byte[] compressed)
        {
            BitArray bits = new BitArray(compressed);
            var compressedBits = new List<bool>(bits.Cast<bool>());
            var compressedInts = BitsToInts(compressedBits);

            var decompressed = Decompress(compressedInts);
            return decompressed;
        }

        public List<int> Compress(byte[] uncompressed)
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            for (int i = 0; i < 256; i++)
                dictionary.Add(i.ToString(), i);
 
            string w = string.Empty;
            List<int> compressed = new List<int>();
 
            foreach (byte c in uncompressed)
            {
                string wc;
                if (!string.IsNullOrEmpty(w))
                    wc = w + ',' + c.ToString();
                else
                    wc = c.ToString();
                if (dictionary.ContainsKey(wc))
                {
                    w = wc;
                }
                else
                {
                    if (!string.IsNullOrEmpty(w))
                        compressed.Add(dictionary[w]);
                    
                    dictionary.Add(wc, dictionary.Count);
                    w = c.ToString();
                }
            }
 
            if (!string.IsNullOrEmpty(w))
                compressed.Add(dictionary[w]);
 
            return compressed;
        }

        public byte[] Decompress(List<int> compressed)
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            for (int i = 0; i < 256; i++)
                dictionary.Add(i, i.ToString());
 
            string w = dictionary[compressed[0]];
            compressed.RemoveAt(0);
            
            List<byte> decompressed = new List<byte>(){w.ToByte()};
 
            foreach (int k in compressed)
            {
                string entry = null;
                if (dictionary.ContainsKey(k))
                    entry = dictionary[k];
                else if (k == dictionary.Count)
                    entry = w + ',' + w.Split(',')[0];

                var itemsInEntry = entry.Split(',');
                foreach (var item in itemsInEntry)
                {
                    decompressed.Add(item.ToByte());
                }
                
                dictionary.Add(dictionary.Count, w + ',' + entry.Split(',')[0]);
 
                w = entry;
            }
 
            return decompressed.ToArray();
        }

        private byte[] ToByteArray(BitArray bitArray)
        {
            byte[] byteArray = new byte[(int)Math.Ceiling((double)bitArray.Length / 8)];
            bitArray.CopyTo(byteArray, 0);

            return byteArray;
        }

        private List<bool> IntsToBits(List<int> list)
        {
            var max = list.Max();
            var bitArraySize = (int)Math.Ceiling(Math.Log(max, 2));

            var intArray = new BitArray(list.ToArray());
            var bitList = new List<bool>();

            // записать число бит на символ
            // в начале файла 11110111... - значит на 1 символ 5 бит 
            for (int i = 0; i < bitArraySize - 1; i++)
            {
                bitList.Add(true);
            }
            bitList.Add(false);
            // записать символы
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < bitArraySize; j++)
                {
                    bitList.Add(intArray[i * 32 + j]); // 32 - количество бит в инте
                }
            }

            return bitList;
        }

        private List<int> BitsToInts(List<bool> array)
        {
            var bitArraySize = array.FindIndex(p => p == false) + 1;
            var buf = new BitArray(bitArraySize);
            int numOfInts = array.Count/bitArraySize;
            int[] arrayInt = new int[numOfInts-1];

            for (int i = 1; i < numOfInts; i++)
            {
                for (int j = 0; j < bitArraySize; j++)
                {
                    buf[j] = array[i*bitArraySize + j];
                }
                buf.CopyTo(arrayInt,i-1);
            }
            return arrayInt.ToList();
        }
    }
}
