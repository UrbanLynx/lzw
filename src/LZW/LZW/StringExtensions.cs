﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZW
{
    public static class StringExtensions
    {
        public static byte ToByte(this string str)
        {
            return Convert.ToByte(str);
        }
    }
}
